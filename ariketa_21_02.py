# -*- coding: utf-8 -*-
"""
asyncio-n sakontzeko:

https://stackoverflow.com/questions/50757497/simplest-async-await-example-possible-in-python
https://djangostars.com/blog/asynchronous-programming-in-python-asyncio/
https://github.com/crazyguitar/pysheeet/blob/master/docs/appendix/python-concurrent.rst

"""
import asyncio
import time


def estandarra():
    def sleep():
        print(f"Time: {time.time() - start:.2f}")
        time.sleep(1)

    def sum(name, numbers):
        total = 0
        for number in numbers:
            print(f"Task {name}: Computing {total}+{number}")
            sleep()
            total += number
        print(f"Task {name}: Sum = {total}\n")

    start = time.time()
    tasks = [
        sum("A", [1, 2]),
        sum("B", [1, 2, 3]),
    ]
    end = time.time()
    print(f"Time: {end-start:.2f} sec")


def asinkronoa():
    async def sleep():
        print(f"Time: {time.time() - start:.2f}")
        await asyncio.sleep(1)

    async def sum(name, numbers):
        total = 0
        for number in numbers:
            print(f"Task {name}: Computing {total}+{number}")
            await sleep()
            total += number
        print(f"Task {name}: Sum = {total}\n")

    start = time.time()

    loop = asyncio.get_event_loop()
    tasks = [
        loop.create_task(sum("A", [1, 2])),
        loop.create_task(sum("B", [1, 2, 3])),
    ]
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()

    end = time.time()
    print(f"Time: {end-start:.2f} sec")


if __name__ == "__main__":
    estandarra()
    print("-------------------------")
    asinkronoa()
