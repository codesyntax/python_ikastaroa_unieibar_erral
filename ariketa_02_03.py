# -*- coding: utf-8 -*-
"""
Datu-motak sortuta ditugula, ekarri ditzagun 1. solairuko ordenagailuen datuak eta modelatu
ditzagun definitu ditugun datu-motekin


1. CSV formatua inportazea csv modulua erabiliz

2. Lerro bakoitzetik Ordenagailua motako elementuak sortu eta zerrenda baten gorde

3. Gela motako elementuak ere sortu (kontuz, elementu bakarrak izan beharko lirateke, hau da, ez sortu bi Gela id berberarekin)
"""
from ariketa_01_01 import Gela
from ariketa_01_01 import Ordenagailua

import csv


def kargatu_datuak(filename):
    ordenagailuak = []
    gelak = {}
    with open(filename) as fp:
        reader = csv.DictReader(fp)
        for i, row in enumerate(reader):
            gela_id = row["Gela"]
            gela = gelak.get(gela_id, None)
            if gela is None:
                gela = Gela(id=gela_id, kodea=str(gela_id), izena=gela_id)
                print(gela)

            ordenagailua = Ordenagailua(
                id=i,
                kodea=row["Ordenagailua"],
                ip_helbidea=row["IP Helbidea"],
                sistema_eragilea=row["Sistema Eragilea"],
                erosketa_eguna=None,
                azken_errebisioa=None,
                oharrak=row["Oharrak"],
            )
            # print(ordenagailua)
            gela.ordenagailuak.append(ordenagailua)
            gelak[gela_id] = gela

    # print(len(gelak.keys()))
    for gela in gelak.values():
        print(
            "{} gelaren ordenagailu kopurua: {}".format(
                gela.id, len(gela.ordenagailuak)
            )
        )


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_1/ordenagailuak.csv")
