# -*- coding: utf-8 -*-
"""

Modu sinplea:

"""

import time
from ariketa_20_01 import funtzio_luze_luzea


if __name__ == "__main__":
    hasiera = time.time()
    funtzio_luze_luzea()
    bukaera = time.time()
    print("funtzioaren iraupena: {} segundo".format(bukaera - hasiera))
