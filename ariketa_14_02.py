# -*- coding: utf-8 -*-
"""
Orain badugu jada solairu guztien informazioa lortzeko modua, kargatu dezagun guztia
gure MySQL datubasera.

Horretarako mysqlclient izeneko modulua erabili beharko dugu.

Aurreragoko adibide baten gertatu bezala, mysqlclient liburutegiak DB-API 2 jarraitzen du, beraz era berean
egin dezakegu lan berarekin

Nahiz eta izena mysqlclient duen, inportatzeko garaian MySQLdb bezala inportatu behar da.

Jatorriz liburutegiaren izena MySQLdb zen baina python3rako ez zuten portatu eta pakete berri
bat atera zuten izen berriarekin baina barruko gauza guztiak berdin utzita.
"""

from ariketa_09_01 import get_solairuak

import MySQLdb

MYSQL_HOST = "127.0.0.1"
MYSQL_USER = "unieibar"
MYSQL_PASS = "unieibar"
MYSQL_DB = "ordenagailuak"


def kargatu_solairuak():
    con = MySQLdb.connect(
        host=MYSQL_HOST, user=MYSQL_USER, passwd=MYSQL_PASS, db=MYSQL_DB
    )
    for solairua in get_solairuak():
        sql = "INSERT INTO SOLAIRUA (KODEA, IZENA) VALUES (%s, %s)"
        cur = con.cursor()

        cur.execute(sql, (str(solairua.kodea), str(solairua.izena)))
        con.commit()
        cur.close()

        sql = "SELECT ID from SOLAIRUA WHERE KODEA=%s AND IZENA=%s"
        cur = con.cursor()
        cur.execute(sql, (str(solairua.kodea), str(solairua.izena)))
        solairua_rows = cur.fetchall()
        for solairua_row in solairua_rows:
            # ID = solairua_row[0]

            for gela in solairua.gelak:
                sql = "INSERT INTO GELA (KODEA, IZENA, SOLAIRUA) VALUES (%s, %s, %s)"
                cur = con.cursor()

                cur.execute(sql, (gela.kodea, gela.izena, solairua_row[0]))
                con.commit()
                cur.close()

                sql = "SELECT ID FROM GELA WHERE KODEA=%s and IZENA=%s and SOLAIRUA=%s"
                cur = con.cursor()
                cur.execute(sql, (gela.kodea, gela.izena, solairua_row[0]))
                gela_rows = cur.fetchall()
                for gela_row in gela_rows:
                    # ID = gela_row[0]
                    for ordenagailua in gela.ordenagailuak:
                        sql = "INSERT INTO ORDENAGAILUA(KODEA, IP_HELBIDEA, SISTEMA_ERAGILEA, EROSKETA_EGUNA, AZKEN_ERREBISIOA, OHARRAK, GELA) VALUES (%s, %s, %s, %s, %s, %s, %s)"
                        cur = con.cursor()
                        cur.execute(
                            sql,
                            (
                                ordenagailua.kodea,
                                ordenagailua.ip_helbidea,
                                ordenagailua.sistema_eragilea,
                                ordenagailua.erosketa_eguna,
                                ordenagailua.azken_errebisioa,
                                ordenagailua.oharrak,
                                gela_row[0],
                            ),
                        )
                        con.commit()
                        cur.close()


if __name__ == "__main__":
    kargatu_solairuak()
