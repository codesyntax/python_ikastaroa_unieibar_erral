# -*- coding: utf-8 -*-
"""
6. solairuko ordenagailuen informazio bakarra beren IP helbidea da. Gela bakarra dago baina beste informaziorik ez dugu.

Zuzenean Gela eta Ordenagailua motako elementuak sortuko ditugu
"""

from ariketa_01_01 import Gela, Ordenagailua, Solairua
from utils import inprimatu_gelak


def kargatu_datuak(filename):
    gelak = {}
    with open(filename) as fp:
        datuak = fp.read()
        ip_helbideak = datuak.split(",")
        gela = Gela(601, "601", "601")

        for i, ip_helbidea in enumerate(ip_helbideak):
            ordenagailua = Ordenagailua(
                id=i, kodea=str(i), ip_helbidea=ip_helbidea.strip()
            )
            gela.ordenagailuak.append(ordenagailua)

        gelak[601] = gela

    inprimatu_gelak(gelak)


def get_gelak(filename):
    gelak = {}
    with open(filename) as fp:
        datuak = fp.read()
        ip_helbideak = datuak.split(",")
        gela = Gela(601, "601", "601")

        for i, ip_helbidea in enumerate(ip_helbideak):
            ordenagailua = Ordenagailua(
                id=i, kodea=str(i), ip_helbidea=ip_helbidea.strip()
            )
            gela.ordenagailuak.append(ordenagailua)

        gelak[601] = gela

    return gelak.values()


def get_solairua():
    solairua = Solairua(id=6, kodea="6", izena="6. solairua")
    solairua.gelak = get_gelak("./datuak/solairua_6/ordenagailuak.txt")
    return solairua


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_6/ordenagailuak.txt")
