# -*- coding: utf-8 -*-
"""
3. solairuan INI fitxategi bat dugu, hori pythoneko configparser moduluarekin irakurri daiteke
"""

import configparser


def kargatu_datuak(filename):
    cp = configparser.ConfigParser()
    cp.read_file(open(filename))

    for section in cp.sections():
        print(section)


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_3/ordenagailuak.ini")
