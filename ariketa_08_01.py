# -*- coding: utf-8 -*-
"""

Solairu bakoitzeko informazioa jada badaukagu, baina momentuz pantailan inprimatzen dira emaitzak

8. ariketa honetan zehar aurreko fitxategiak moldatuko ditugu, bakoitzak solairu bakoitzeko informazioa itzuli dezan
baina ez pantailan inprimatuta, baizik eta get_solairua izeneko funtzio bat eraibiliz egiteko.

hau da, guk bukaeran horrelako zerbait egin beharko genuke:

# 1. solairua
from ariketa_08_02 import get_solairua
solairua_1 = get_solairua()

# 2. solairua
from ariketa_08_03 import get_solairua
solairua_2 = get_solairua()

# 3. solairua
from ariketa_08_04 import get_solairua
solairua_3 = get_solairua()

# 4. solairua
from ariketa_08_05 import get_solairua
solairua_4 = get_solairua()

# 5. solairua
from ariketa_08_06 import get_solairua
solairua_5 = get_solairua()

# 6. solairua
from ariketa_08_07 import get_solairua
solairua_5 = get_solairua()

Oinarri bezala hartu aurreko ariketetako **azken fitxategia** eta bertan funtzio berri bat gehitu get_solairua izenekoa, erabili oinarri gisa
jada existitzen den kargatu_datuak funtzioa, aldatu izena eta inprimatu beharrean itzuli dezala gelen informazioa

Eta bukaeran utils fitxategian inprimatu_solairua(solairua) erako funtzio bat egingo dugu solairuaren informazioa inprimatzeko

"""

from utils import inprimatu_solairua
from ariketa_08_02 import get_solairua as solairua_1
from ariketa_08_03 import get_solairua as solairua_2
from ariketa_08_04 import get_solairua as solairua_3
from ariketa_08_05 import get_solairua as solairua_4
from ariketa_08_06 import get_solairua as solairua_5
from ariketa_08_07 import get_solairua as solairua_6


def inprimatu_solairuak():
    # 1. solairua
    solairua_1 = get_solairua_1()

    # 2. solairua
    solairua_2 = get_solairua_2()

    # 3. solairua
    solairua_3 = get_solairua_3()

    # 4. solairua
    solairua_4 = get_solairua_4()

    # 5. solairua
    solairua_5 = get_solairua_5()

    # 6. solairua
    solairua_6 = get_solairua_6()

    inprimatu_solairua(solairua_1)
    inprimatu_solairua(solairua_2)
    inprimatu_solairua(solairua_3)
    inprimatu_solairua(solairua_4)
    inprimatu_solairua(solairua_5)
    inprimatu_solairua(solairua_6)


if __name__ == "__main__":
    inprimatu_solairuak()
