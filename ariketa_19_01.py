# -*- coding: utf-8 -*-
"""

1. ariketan ikusi genuen klase bat @dataclass dekoradorea erabiliz definitzerakoan klase bateko atributuen
datu-motak definitu daitezkeela.

Baina pythonek mota-dinamikoak erabiltzen dituenez, inon ez du datu-mota horien balidaziorik egiten.

Hala ere, garapen eta aplikazio batzuetan datu-motak definitzeak gauzak erraztu ditzake, adibidez aplikazio handi bat
egiten garatzaile asko baldin badaude lanean, edo oso gutxitan aldatzen den aplikazio bat dugunean, metodo bakoitzak
zer itzultzen duen era erraz baten jakiteko.

Baina pythonek datu-mota hauek definitu arren berauek erabiltzea derrigortzen ez badu, zertarako balio du?

Beno, ba alde batetik kodearen analisi estatikoa egiteko, nahiz eta pythonek berak datu-motak ez egiaztatu, gure garapen
katean linter bat edo datu-moten egiaztatzaile bat izan dezakegu eta berak egin analisi horiek.

Bestetik IDEak erabili ditzake garatzen gaudela laguntza emateko, eta funtzio edo metodo bati deitzen diogunean parametroen
inguruko informazioa emateko.

Pythoneko datu-moten definizio hau TypeScripten antzera egiten da. Ikusi adibide hauek:

"""


def agurra(izena: str) -> str:
    """
    Zera daukagu hemen:
     'izena' izeneko 'string' parametro bat jasotzen duen funtzioa eta 'string' motako zerbait itzultzen duena (->)
    """
    return "Kaixo " + izena


print(agurra("Mikel"))


"""
Horrela, datu-mota konposatuak ere osatu ditzakegu, adibidez, bektore bat zenbaki osoen zerrenda gisa definitu dezakegu
eta gero Bektorea erabili moten definizioan
"""

Bektorea = list[int]


def batuketa(datuak: Bektorea) -> int:
    return sum(datuak)


zenbakia = batuketa([1, 2, 3, 4, 5])
print(zenbakia)

"""
Konposizio hau erabiliz datu-motak nahi adina konplikatu/sinplifikatu ditzakegu:

Hemen pythonen dokumentazioko adibide bat
"""

from collections.abc import Sequence

ConnectionOptions = dict[str, str]
Address = tuple[str, int]
Server = tuple[Address, ConnectionOptions]


def broadcast_message(message: str, servers: Sequence[Server]) -> None:
    pass


# The static type checker will treat the previous type signature as
# being exactly equivalent to this one.
def broadcast_message(
    message: str, servers: Sequence[tuple[tuple[str, int], dict[str, str]]]
) -> None:
    pass


"""
Esango nuke, hala ere, typehint hauen erabilera nahiko mugatua dela gure ingurunean, web ingurunean.

Agian aplikazio zientifiko eta matematikoetan garrantzitsuagoa izan daiteke (scipy/numpy erabiltzean)

Guk behintzat ez dugu erabiltzen.
"""
