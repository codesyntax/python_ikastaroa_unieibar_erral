# -*- coding: utf-8 -*-
"""

4. solairuko informazioa JSON fitxategi baten dago. Hori pythonen json izeneko moduluarekin kudeatzen
da eta fitxategia irakurrita datu guztiak pythoneko zerrenda eta hiztegien bidez modelatuko dizkigu
automatikoki

Sortu ditzagun zuzenean Gela eta Ordenagailua datu-motak eta inprimatu

"""
import json
from ariketa_01_01 import Gela, Ordenagailua, Solairua
from utils import inprimatu_gelak, data_prozesatu


def kargatu_datuak(filename):
    datuak = json.load(open(filename))
    gelak = {}
    for ordenagailu_data in datuak:
        gela_id = ordenagailu_data["room"]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=str(gela_id))

        ordenagailua = Ordenagailua(
            id=ordenagailu_data["id"],
            kodea=str(ordenagailu_data["id"]),
            ip_helbidea=ordenagailu_data["ip"],
            sistema_eragilea=ordenagailu_data["os"],
            erosketa_eguna=data_prozesatu(ordenagailu_data["buydate"]),
            azken_errebisioa=data_prozesatu(ordenagailu_data["revisiondate"]),
            oharrak=ordenagailu_data["notes"],
        )

        gela.ordenagailuak.append(ordenagailua)

        gelak[gela_id] = gela

    inprimatu_gelak(gelak)


def data_prozesatu(value):
    """
    dataren itxura duen zerbait prozesatu behar dugu
    """

    return None


def get_gelak(filename):
    datuak = json.load(open(filename))
    gelak = {}
    for ordenagailu_data in datuak:
        gela_id = ordenagailu_data["room"]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=str(gela_id))

        ordenagailua = Ordenagailua(
            id=ordenagailu_data["id"],
            kodea=str(ordenagailu_data["id"]),
            ip_helbidea=ordenagailu_data["ip"],
            sistema_eragilea=ordenagailu_data["os"],
            erosketa_eguna=data_prozesatu(ordenagailu_data["buydate"]),
            azken_errebisioa=data_prozesatu(ordenagailu_data["revisiondate"]),
            oharrak=ordenagailu_data["notes"],
        )

        gela.ordenagailuak.append(ordenagailua)

        gelak[gela_id] = gela

    return gelak.values()


def get_solairua():
    solairua = Solairua(id=4, kodea="4", izena="4. solairua")
    solairua.gelak = get_gelak("./datuak/solairua_4/ordenagailuak.json")
    return solairua


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_4/ordenagailuak.json")
