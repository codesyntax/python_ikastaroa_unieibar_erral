# -*- coding: utf-8 -*-
"""
Datu-motak sortuta ditugula, ekarri ditzagun 1. solairuko ordenagailuen datuak eta modelatu
ditzagun definitu ditugun datu-motekin


1. CSV formatua inportatzea, csv liburutegia erabiliz

2. Inprimatu lerro bakoitzaren edukiak.
"""

import csv


def kargatu_datuak(filename):
    # datuak-fitxategitik kargatu
    with open(filename) as fp:
        reader = csv.DictReader(fp)
        for row in reader:
            print(row)


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_1/ordenagailuak.csv")
