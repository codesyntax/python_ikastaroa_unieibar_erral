from geolokalizazioa_02 import (
    lortu_geo_informazioa,
    lortu_eguraldi_informazioa,
    erakutsi_geo_informazioa,
)
from hardware import (
    eguneratu_pantaila,
    set_berogailua,
    set_aire_girotua,
)

if __name__ == "__main__":
    herria = input("Idatzi bilatu beharreko herriaren izena: ")
    datuak = lortu_geo_informazioa(herria)
    erakutsi_geo_informazioa(datuak)
    eguraldi_datuak = lortu_eguraldi_informazioa(
        datuak.get('latitude'), datuak.get('longitude')
    )
    # akzioak abiarazi: kasu honetan, soilik pantaila eguneratu
    graduak = eguraldi_datuak.get('current_weather', {}).get('temperature')
    eguneratu_pantaila(graduak)