# -*- coding: utf-8 -*-
"""
5. solairuko informazioa XML fitxategi baten dator.

Pythonen xml moduluak XML fitxategiak irakurri eta idazteko oinarrizko funtzionalitateak eskaintzen dizkigu
"""

from xml.etree import ElementTree


def kargatu_datuak(filename):
    tree = ElementTree.parse(filename)
    root = tree.getroot()
    print(root.tag)
    for gela in root:
        print(" {}".format(gela.get("kodea")))
        for ordenagailua in gela:
            print("   {}".format(ordenagailua.get("id")))
            print("     {}".format(ordenagailua.find("ip").text))
            print("     {}".format(ordenagailua.find("se").text))


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_5/ordenagailuak.xml")
