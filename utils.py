# -*- coding: utf-8 -*-
import datetime


def inprimatu_gelak(gelak):
    """ kasu honetan `gelak` hiztegi bat da """
    for gela in gelak.values():
        print(
            "{} gelaren ordenagailu kopurua: {}".format(
                gela.id, len(gela.ordenagailuak)
            )
        )


def inprimatu_gela_zerrenda(gelak):
    """ kasu honetan `gelak` zerrenda bat da """
    for gela in gelak:
        print(
            "{} gelaren ordenagailu kopurua: {}".format(
                gela.id, len(gela.ordenagailuak)
            )
        )


def inprimatu_solairua(solairua):
    print("Solairua: {}".format(solairua.izena))
    # solairua.gelak zerrenda bat da ez hiztegi bat
    inprimatu_gela_zerrenda(solairua.gelak)


def data_prozesatu(value):
    """
    dataren itxura duen zerbait prozesatu behar dugu
    """
    try:
        data = datetime.datetime.strptime(value, "%Y-%m-%d")
    except ValueError:
        data = None
    return data
