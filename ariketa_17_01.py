# -*- coding: utf-8 -*-
"""
Datuak deskargatzeko, liburutegi estandarreko urllib erabili beharrean
requests izeneko liburutegia erabiliko dugu askoz erosoagoa delako
"""


from ariketa_01_01 import Gela
from ariketa_01_01 import Ordenagailua
from ariketa_01_01 import Solairua

import csv
import requests
import codecs
from utils import inprimatu_gela_zerrenda


BASE_URL = "http://localhost:8000"


def kargatu_datuak(filename):
    ordenagailuak = []
    gelak = {}
    with open(filename) as fp:
        reader = csv.DictReader(fp)
        for i, row in enumerate(reader):
            gela_id = row["Gela"]
            gela = gelak.get(gela_id, None)
            if gela is None:
                gela = Gela(id=gela_id, kodea=str(gela_id), izena=gela_id)
                print(gela)

            ordenagailua = Ordenagailua(
                id=i,
                kodea=row["Ordenagailua"],
                ip_helbidea=row["IP Helbidea"],
                sistema_eragilea=row["Sistema Eragilea"],
                erosketa_eguna=None,
                azken_errebisioa=None,
                oharrak=row["Oharrak"],
            )
            # print(ordenagailua)
            gela.ordenagailuak.append(ordenagailua)
            gelak[gela_id] = gela

    # print(len(gelak.keys()))
    for gela in gelak.values():
        print(
            "{} gelaren ordenagailu kopurua: {}".format(
                gela.id, len(gela.ordenagailuak)
            )
        )


def get_gelak(filename):
    ordenagailuak = []
    gelak = {}
    with open(filename) as fp:
        reader = csv.DictReader(fp)
        for i, row in enumerate(reader):
            gela_id = row["Gela"]
            gela = gelak.get(gela_id, None)
            if gela is None:
                gela = Gela(id=gela_id, kodea=str(gela_id), izena=gela_id)
                print(gela)

            ordenagailua = Ordenagailua(
                id=i,
                kodea=row["Ordenagailua"],
                ip_helbidea=row["IP Helbidea"],
                sistema_eragilea=row["Sistema Eragilea"],
                erosketa_eguna=None,
                azken_errebisioa=None,
                oharrak=row["Oharrak"],
            )
            # print(ordenagailua)
            gela.ordenagailuak.append(ordenagailua)
            gelak[gela_id] = gela

    return gelak.values()


def deskargatu_datuak(helbidea):
    results = requests.get(helbidea)
    # Zergatik split('\n'): testua zuzenean pasatzen dugulako, 9. ariketan irekitako fitxategia
    # pasatzen dugu eta han DictReader arduratzen da testua lerroz lerro irakurtzeaz
    # hemen guk "forzatu" behar dugu irakurketa, lerro bukaerako karakterea
    # banatzaile gisa erabilita
    text = csv.DictReader(results.text.split("\n"))

    gelak = {}
    for i, row in enumerate(text):
        gela_id = row["Gela"]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=gela_id)
            print(gela)

        ordenagailua = Ordenagailua(
            id=i,
            kodea=row["Ordenagailua"],
            ip_helbidea=row["IP Helbidea"],
            sistema_eragilea=row["Sistema Eragilea"],
            erosketa_eguna=None,
            azken_errebisioa=None,
            oharrak=row["Oharrak"],
        )
        # print(ordenagailua)
        gela.ordenagailuak.append(ordenagailua)
        gelak[gela_id] = gela

    return gelak.values()


def get_solairua():
    solairua = Solairua(id=1, kodea="1", izena="1. solairua")
    solairua.gelak = deskargatu_datuak(f"{BASE_URL}/ordenagailuak.csv")
    return solairua


if __name__ == "__main__":
    solairua = get_solairua()
    inprimatu_gela_zerrenda(solairua.gelak)
