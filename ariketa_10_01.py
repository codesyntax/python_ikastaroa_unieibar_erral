# -*- coding: utf-8 -*-
"""
Deskargatutako fitxategiak lokalean karpeta baten gorde eta sistema tar komandoa erabiliz konprimatu (subprocess, os eta sys moduluak).

Saltatu egingo dugu fitxategien prozesamentua
"""
import tempfile
import urllib.request
import subprocess


BASE_URL = 'http://localhost:8000'

solairua_1_file = 'ordenagailuak.csv'
solairua_2_file = 'ordenagailuak.db'
solairua_3_file = 'ordenagailuak.ini'
solairua_4_file = 'ordenagailuak.json'
solairua_5_file = 'ordenagailuak.xml'
solairua_6_file = 'ordenagailuak.txt'


def deskargatu_fitxategiak(fitxategiak):
    # lehenengo karpeta tenporal bat sortuko dugu eta hara deskargatu
    # Kontuz ze erabiltzeari uzten diogunean direktorio tenporala ezabatu egingo da.
    directory = tempfile.TemporaryDirectory()

    for fitxategia in fitxategiak:
        sock = urllib.request.urlopen(f'{BASE_URL}/{fitxategia}')
        with open(directory.name + '/' + fitxategia, 'wb') as fp:
            fp.write(sock.read())

    print(directory.name)
    subprocess.run(['tar', 'cfz', '/tmp/fitxategiak.tgz', directory.name])



if __name__ == '__main__':
    fitxategiak = [solairua_1_file, solairua_2_file, solairua_3_file, solairua_4_file, solairua_5_file, solairua_6_file]
    deskargatu_fitxategiak(fitxategiak)
