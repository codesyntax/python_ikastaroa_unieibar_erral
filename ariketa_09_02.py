# -*- coding: utf-8 -*-
"""
Datu-motak sortuta ditugula, ekarri ditzagun 1. solairuko ordenagailuen datuak eta modelatu
ditzagun definitu ditugun datu-motekin


1. CSV formatua inportazea csv modulua erabiliz

2. Lerro bakoitzetik Ordenagailua motako elementuak sortu eta zerrenda baten gorde
"""

from ariketa_01_01 import Gela
from ariketa_01_01 import Ordenagailua
from ariketa_01_01 import Solairua

import csv
import urllib.request
import codecs
from utils import inprimatu_gela_zerrenda


BASE_URL = "http://localhost:8000"


def kargatu_datuak(filename):
    ordenagailuak = []
    gelak = {}
    with open(filename) as fp:
        reader = csv.DictReader(fp)
        for i, row in enumerate(reader):
            gela_id = row["Gela"]
            gela = gelak.get(gela_id, None)
            if gela is None:
                gela = Gela(id=gela_id, kodea=str(gela_id), izena=gela_id)
                print(gela)

            ordenagailua = Ordenagailua(
                id=i,
                kodea=row["Ordenagailua"],
                ip_helbidea=row["IP Helbidea"],
                sistema_eragilea=row["Sistema Eragilea"],
                erosketa_eguna=None,
                azken_errebisioa=None,
                oharrak=row["Oharrak"],
            )
            # print(ordenagailua)
            gela.ordenagailuak.append(ordenagailua)
            gelak[gela_id] = gela

    # print(len(gelak.keys()))
    for gela in gelak.values():
        print(
            "{} gelaren ordenagailu kopurua: {}".format(
                gela.id, len(gela.ordenagailuak)
            )
        )


def get_gelak(filename):
    ordenagailuak = []
    gelak = {}
    with open(filename) as fp:
        reader = csv.DictReader(fp)
        for i, row in enumerate(reader):
            gela_id = row["Gela"]
            gela = gelak.get(gela_id, None)
            if gela is None:
                gela = Gela(id=gela_id, kodea=str(gela_id), izena=gela_id)
                print(gela)

            ordenagailua = Ordenagailua(
                id=i,
                kodea=row["Ordenagailua"],
                ip_helbidea=row["IP Helbidea"],
                sistema_eragilea=row["Sistema Eragilea"],
                erosketa_eguna=None,
                azken_errebisioa=None,
                oharrak=row["Oharrak"],
            )
            # print(ordenagailua)
            gela.ordenagailuak.append(ordenagailua)
            gelak[gela_id] = gela

    return gelak.values()


def deskargatu_datuak(helbidea):
    # Oharra: Ongi Etorri Pythonen bytes vs. string borrokara
    # Normalean sarrera/irteerarekin lan egiten dugunean datuak
    # bytes formatuan etorriko dira, eta pythonen barruko eragiketak
    # str formatuarekin egin behar dira.
    # batetik bestera bihurtzeko encode eta decode funtzioak edo
    # bytes eta str casting funtzioan erabili behar dira.
    #
    # Hau da:
    #
    #  b'bytes' -> decode -> 'string'
    #
    #  'string' -> encode -> b'bytes'
    #
    # fijatyu bytes motaren hasierako b horretan, horrek string
    # bat bytes motako bihurtzen du
    #
    sock = urllib.request.urlopen(helbidea)
    # Hau era jasangarrian helbidearen edukia irakurtzeko modua da
    # Beste modu bat hauxe litzateke:
    #
    # data_bytes = sock.read()
    # data_text = data_bytes.decode('utf-8')
    # reader = csv.DictReader(data_text)
    #
    # Honen arazoa zera da: sock.read() egiten dugunean datuak memoriara kargatzen
    # ditugu eta fitxategi handiekin horrek arazoak ekarri ditzake
    # codecs.iterdecode erabiliz fitxategia irakurri ahala bihurtzen da stringera
    # eta csv.DictReaderrek ere fitxategia behar ahala irakurtzen duenez
    # memoriaren erabilera jasangarriagoa egiten du.
    reader = csv.DictReader(codecs.iterdecode(sock, "utf-8"))

    gelak = {}
    for i, row in enumerate(reader):
        gela_id = row["Gela"]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=gela_id)
            print(gela)

        ordenagailua = Ordenagailua(
            id=i,
            kodea=row["Ordenagailua"],
            ip_helbidea=row["IP Helbidea"],
            sistema_eragilea=row["Sistema Eragilea"],
            erosketa_eguna=None,
            azken_errebisioa=None,
            oharrak=row["Oharrak"],
        )
        # print(ordenagailua)
        gela.ordenagailuak.append(ordenagailua)
        gelak[gela_id] = gela

    return gelak.values()


def get_solairua():
    solairua = Solairua(id=1, kodea="1", izena="1. solairua")
    solairua.gelak = deskargatu_datuak(f"{BASE_URL}/ordenagailuak.csv")
    return solairua


if __name__ == "__main__":
    solairua = get_solairua()
    inprimatu_gela_zerrenda(solairua.gelak)
