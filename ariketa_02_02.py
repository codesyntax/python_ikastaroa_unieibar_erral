# -*- coding: utf-8 -*-
"""
Datu-motak sortuta ditugula, ekarri ditzagun 1. solairuko ordenagailuen datuak eta modelatu
ditzagun definitu ditugun datu-motekin


1. CSV formatua inportazea csv modulua erabiliz

2. Lerro bakoitzetik Ordenagailua motako elementuak sortu eta zerrenda batean gorde
"""
import csv
from ariketa_01_01 import Ordenagailua


def kargatu_datuak(filename):
    ordenagailuak = []
    with open(filename) as fp:
        reader = csv.DictReader(fp)
        for i, row in enumerate(reader):
            ordenagailua = Ordenagailua(
                id=i,
                kodea=row["Ordenagailua"],
                ip_helbidea=row["IP Helbidea"],
                sistema_eragilea=row["Sistema Eragilea"],
                erosketa_eguna=None,
                azken_errebisioa=None,
                oharrak=row["Oharrak"],
            )
            print(ordenagailua)
            ordenagailuak.append(ordenagailua)

    print(len(ordenagailuak))


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_1/ordenagailuak.csv")
