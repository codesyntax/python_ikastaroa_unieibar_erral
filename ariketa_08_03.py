# -*- coding: utf-8 -*-
"""
SQLite datubasetik sortu orain Gela eta Ordenagailua objektuak

erabili 2. ariketako 3. fitxategiak erabili dugun hiztegia elementuak gordetzeko

"""


import sqlite3
from ariketa_01_01 import Gela, Ordenagailua, Solairua


def kargatu_datuak(filename):
    con = sqlite3.connect(filename)
    cur = con.cursor()
    cur.execute("SELECT * FROM ORDENAGAILUAK")
    rows = cur.fetchall()

    gelak = {}

    for row in rows:
        gela_id = row[1]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=str(gela_id))
            print(gela)

        ordenagailua = Ordenagailua(
            id=row[0],
            kodea=str(row[0]),
            ip_helbidea=row[2],
            sistema_eragilea=row[2],
            oharrak=row[4],
        )

        gela.ordenagailuak.append(ordenagailua)
        gelak[gela_id] = gela

    for gela in gelak.values():
        print(
            "{} gelaren ordenagailu kopurua: {}".format(
                gela.id, len(gela.ordenagailuak)
            )
        )


def get_gelak(filename):
    con = sqlite3.connect(filename)
    cur = con.cursor()
    cur.execute("SELECT * FROM ORDENAGAILUAK")
    rows = cur.fetchall()

    gelak = {}

    for row in rows:
        gela_id = row[1]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=str(gela_id))
            print(gela)

        ordenagailua = Ordenagailua(
            id=row[0],
            kodea=str(row[0]),
            ip_helbidea=row[2],
            sistema_eragilea=row[2],
            oharrak=row[4],
        )

        gela.ordenagailuak.append(ordenagailua)
        gelak[gela_id] = gela

    return gelak.values()


def get_solairua():
    solairua = Solairua(id=2, kodea="2", izena="2. solairua")
    solairua.gelak = get_gelak("./datuak/solairua_2/ordenagailuak.db")
    return solairua


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_2/ordenagailuak.db")
