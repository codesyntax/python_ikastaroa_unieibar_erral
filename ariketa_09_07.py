# -*- coding: utf-8 -*-
"""
6. solairuko ordenagailuen informazio bakarra beren IP helbidea da. Gela bakarra dago baina beste informaziorik ez dugu.

Zuzenean Gela eta Ordenagailua motako elementuak sortuko ditugu
"""

from ariketa_01_01 import Gela
from ariketa_01_01 import Ordenagailua
from ariketa_01_01 import Solairua
from utils import inprimatu_gelak, inprimatu_gela_zerrenda

import urllib.request


BASE_URL = 'http://localhost:8000'

def kargatu_datuak(filename):
    gelak = {}
    with open(filename) as fp:
        datuak = fp.read()
        ip_helbideak = datuak.split(",")
        gela = Gela(601, "601", "601")

        for i, ip_helbidea in enumerate(ip_helbideak):
            ordenagailua = Ordenagailua(
                id=i, kodea=str(i), ip_helbidea=ip_helbidea.strip()
            )
            gela.ordenagailuak.append(ordenagailua)

        gelak[601] = gela

    inprimatu_gelak(gelak)


def get_gelak(filename):
    gelak = {}
    with open(filename) as fp:
        datuak = fp.read()
        ip_helbideak = datuak.split(",")
        gela = Gela(601, "601", "601")

        for i, ip_helbidea in enumerate(ip_helbideak):
            ordenagailua = Ordenagailua(
                id=i, kodea=str(i), ip_helbidea=ip_helbidea.strip()
            )
            gela.ordenagailuak.append(ordenagailua)

        gelak[601] = gela

    return gelak.values()


def deskargatu_datuak(helbidea):
    sock = urllib.request.urlopen(helbidea)
    datuak = sock.read().decode('utf-8')
    ip_helbideak = datuak.split(",")
    gela = Gela(601, "601", "601")
    gelak = {}
    for i, ip_helbidea in enumerate(ip_helbideak):
        ordenagailua = Ordenagailua(
            id=i, kodea=str(i), ip_helbidea=ip_helbidea.strip()
        )
        gela.ordenagailuak.append(ordenagailua)

    gelak[601] = gela

    return gelak.values()



def get_solairua():
    solairua = Solairua(id=6, kodea="6", izena="6. solairua")
    solairua.gelak = deskargatu_datuak(f"{BASE_URL}/ordenagailuak.txt")
    return solairua


if __name__ == "__main__":
    solairua = get_solairua()
    inprimatu_gela_zerrenda(solairua.gelak)
