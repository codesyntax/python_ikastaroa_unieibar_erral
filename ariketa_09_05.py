# -*- coding: utf-8 -*-
"""

4. solairuko informazioa JSON fitxategi baten dago. Hori pythonen json izeneko moduluarekin kudeatzen
da eta fitxategia irakurrita datu guztiak pythoneko zerrenda eta hiztegien bidez modelatuko dizkigu
automatikoki

Sortu ditzagun zuzenean Gela eta Ordenagailua datu-motak eta inprimatu

"""
from ariketa_01_01 import Gela
from ariketa_01_01 import Ordenagailua
from ariketa_01_01 import Solairua
from utils import inprimatu_gela_zerrenda
from utils import inprimatu_gelak

import json
import codecs
import urllib.request

BASE_URL = 'http://localhost:8000'
def kargatu_datuak(filename):
    datuak = json.load(open(filename))
    gelak = {}
    for ordenagailu_data in datuak:
        gela_id = ordenagailu_data["room"]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=str(gela_id))

        ordenagailua = Ordenagailua(
            id=ordenagailu_data["id"],
            kodea=str(ordenagailu_data["id"]),
            ip_helbidea=ordenagailu_data["ip"],
            sistema_eragilea=ordenagailu_data["os"],
            erosketa_eguna=ordenagailu_data["buydate"],
            azken_errebisioa=ordenagailu_data["revisiondate"],
            oharrak=ordenagailu_data["notes"],
        )

        gela.ordenagailuak.append(ordenagailua)

        gelak[gela_id] = gela

    inprimatu_gelak(gelak)


def data_prozesatu(value):
    """
    dataren itxura duen zerbait prozesatu behar dugu
    """

    return None


def get_gelak(filename):
    datuak = json.load(open(filename))
    gelak = {}
    for ordenagailu_data in datuak:
        gela_id = ordenagailu_data["room"]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=str(gela_id))

        ordenagailua = Ordenagailua(
            id=ordenagailu_data["id"],
            kodea=str(ordenagailu_data["id"]),
            ip_helbidea=ordenagailu_data["ip"],
            sistema_eragilea=ordenagailu_data["os"],
            erosketa_eguna=data_prozesatu(ordenagailu_data["buydate"]),
            azken_errebisioa=data_prozesatu(ordenagailu_data["revisiondate"]),
            oharrak=ordenagailu_data["notes"],
        )

        gela.ordenagailuak.append(ordenagailua)

        gelak[gela_id] = gela

    return gelak.values()


def deskargatu_datuak(helbidea):
    sock = urllib.request.urlopen(helbidea)
    # json modulua berezia da, nahiz eta sock-ek bytes erako
    # datuak itzuli hauek ulertzeko gai da.
    datuak = json.load(sock)
    gelak = {}
    for ordenagailu_data in datuak:
        gela_id = ordenagailu_data["room"]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=str(gela_id))

        ordenagailua = Ordenagailua(
            id=ordenagailu_data["id"],
            kodea=str(ordenagailu_data["id"]),
            ip_helbidea=ordenagailu_data["ip"],
            sistema_eragilea=ordenagailu_data["os"],
            erosketa_eguna=data_prozesatu(ordenagailu_data["buydate"]),
            azken_errebisioa=data_prozesatu(ordenagailu_data["revisiondate"]),
            oharrak=ordenagailu_data["notes"],
        )

        gela.ordenagailuak.append(ordenagailua)

        gelak[gela_id] = gela

    return gelak.values()


def get_solairua():
    solairua = Solairua(id=4, kodea="4", izena="4. solairua")
    solairua.gelak = deskargatu_datuak(f"{BASE_URL}/ordenagailuak.json")
    return solairua


if __name__ == "__main__":
    solairua = get_solairua()
    inprimatu_gela_zerrenda(solairua.gelak)
