# -*- coding: utf-8 -*-
"""
Fitxategiak http eskaera bat egitean lokalean karpeta baten gorde.
Fitxategi guzti horiek konprimatutako fitxategi baten gorde behar dira python bakarrik erabiliz (gzip edo zipfile modulua)

Saltatu egingo dugu fitxategien prozesamentua
"""
from io import BytesIO
import tempfile
import urllib.request
import subprocess

import zipfile

BASE_URL = 'http://localhost:8000'

solairua_1_file = 'ordenagailuak.csv'
solairua_2_file = 'ordenagailuak.db'
solairua_3_file = 'ordenagailuak.ini'
solairua_4_file = 'ordenagailuak.json'
solairua_5_file = 'ordenagailuak.xml'
solairua_6_file = 'ordenagailuak.txt'


def deskargatu_fitxategiak(fitxategiak):
    # lehenengo karpeta tenporal bat sortuko dugu eta hara deskargatu
    # Kontuz ze erabiltzeari uzten diogunean direktorio tenporala ezabatu egingo da.
    directory = tempfile.TemporaryDirectory()

    for fitxategia in fitxategiak:
        sock = urllib.request.urlopen(f'{BASE_URL}/{fitxategia}')
        with open(directory.name + '/' + fitxategia, 'wb') as fp:
            fp.write(sock.read())

    # s = BytesIO()

    #with zopefile.ZipFile(s, 'w') as zf:


    konprimatutako_fitxategia = zipfile.ZipFile("/tmp/konprimatutakoa.zip", 'w')
    for filename in fitxategiak:
        konprimatutako_fitxategia.write(directory.name + '/' + filename)





if __name__ == '__main__':
    fitxategiak = [solairua_1_file, solairua_2_file, solairua_3_file, solairua_4_file, solairua_5_file, solairua_6_file]
    deskargatu_fitxategiak(fitxategiak)
