# -*- coding: utf-8 -*-
"""
3. solairuan INI fitxategi bat dugu, hori pythoneko configparser moduluarekin irakurri daiteke


Inprimatu ditzagun balioak era txukun baten


"""

import configparser


def print_section_values(section):
    """
    Berdin zaigu sekzio bat zeri buruzkoa den, ini formatuak ez du horren berri ematen
    guk "asmatu" behar dugu, gure abstrakzio kontu bat da, beraz funtzio hau erabiliz
    inprimatu ditzagun era txukun baten sekzio baten balioak
    """
    print(section.name)
    for key in section.keys():
        print("    {} = {}".format(key, section[key]))


def kargatu_datuak(filename):
    cp = configparser.ConfigParser()
    cp.read_file(open(filename))

    for section in cp.sections():
        if section.startswith("ordenadores"):
            # hau sekzio orokor bat da
            print_section_values(cp[section])
        else:
            # hau ordenagailuaren informazioa ematen duen sekzioa da
            print_section_values(cp[section])


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_3/ordenagailuak.ini")
