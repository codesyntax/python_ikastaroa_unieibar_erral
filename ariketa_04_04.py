# -*- coding: utf-8 -*-
"""
3. solairuan INI fitxategi bat dugu, hori pythoneko configparser moduluarekin irakurri daiteke


Sortu ditzagun Gela eta Ordenagailu motako elementuak


Behin eta berriz erabiltzen ari gara gelak eta ordenagailuak inprimatzeko funtzio bat,
sortu dezakegu fitxategi laguntzaile baten horretarako funtzioa eta erabili hori:


"""

from ariketa_01_01 import Gela
from ariketa_01_01 import Ordenagailua
from utils import inprimatu_gelak

import configparser
import datetime


def print_section_values(section):
    """
    Berdin zaigu sekzio bat zeri buruzkoa den, ini formatuak ez du horren berri ematen
    guk "asmatu" behar dugu, gure abstrakzio kontu bat da, beraz funtzio hau erabiliz
    inprimatu ditzagun era txukun baten sekzio baten balioak
    """
    print(section.name)
    for key in section.keys():
        print("    {} = {}".format(key, section[key]))


def data_prozesatu(value):
    """
    dataren itxura duen zerbait prozesatu behar dugu
    """
    return datetime.datetime.strptime(value, "%Y-%m-%d")


def kargatu_datuak(filename):
    cp = configparser.ConfigParser()
    cp.read_file(open(filename))
    gelak = {}
    for section in cp.sections():
        if section.startswith("ordenadores"):
            # hau sekzio orokor bat da
            print_section_values(cp[section])
        else:
            # hau ordenagailuaren informazioa ematen duen sekzioa da
            # print_section_values(cp[section])
            ordenagailu_info = cp[section]
            gela_id = ordenagailu_info["sala"]
            gela = gelak.get(gela_id, None)
            if gela is None:
                gela = Gela(gela_id, str(gela_id), str(gela_id))

            ordenagailua = Ordenagailua(
                id=section,
                kodea=str(section),
                ip_helbidea=ordenagailu_info["ip"],
                sistema_eragilea=ordenagailu_info["so"],
                erosketa_eguna=data_prozesatu(ordenagailu_info["compra"]),
                azken_errebisioa=data_prozesatu(ordenagailu_info["revision"]),
                oharrak=ordenagailu_info["notas"],
            )
            gela.ordenagailuak.append(ordenagailua)
            gelak[gela_id] = gela

    inprimatu_gelak(gelak)


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_3/ordenagailuak.ini")
