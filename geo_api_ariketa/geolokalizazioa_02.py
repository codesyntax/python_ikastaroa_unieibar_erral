import requests

def lortu_geo_informazioa(herria):
    hiztegia = None
    api_url = "https://geocoding-api.open-meteo.com/v1/search?name={}".format(herria)
    eskaera = requests.get(api_url)
    if eskaera.status_code == 200:
        datuak = eskaera.json()
        zerrenda = datuak.get('results', [])
        if len(zerrenda) > 0:
            hiztegia = zerrenda[0]
        else:
            print("Ezin izan da informaziorik lortu")
    return hiztegia

def lortu_eguraldi_informazioa(lat, lon):
    datuak = None
    api_url = "https://api.open-meteo.com/v1/forecast?latitude={}&longitude={}&current_weather=true".format(
        lat, lon
    )
    eskaera = requests.get(api_url)
    if eskaera.status_code == 200:
        datuak = eskaera.json()
    return datuak

def erakutsi_geo_informazioa(hiztegia):
    print("Herria: {}".format(hiztegia.get('name')))
    print("Lat: {} / Lon: {}".format(hiztegia.get('latitude'), hiztegia.get('longitude')))

def erakutsi_eguraldi_informazioa(datuak):
    print("Tenperatura: {} ºC".format(
        datuak.get('current_weather', {}).get('temperature')
    ))

if __name__ == "__main__":
    herria = input("Idatzi bilatu beharreko herriaren izena: ")
    datuak = lortu_geo_informazioa(herria)
    erakutsi_geo_informazioa(datuak)
    eguraldi_datuak = lortu_eguraldi_informazioa(
        datuak.get('latitude'), datuak.get('longitude')
    )
    erakutsi_eguraldi_informazioa(eguraldi_datuak)