# -*- coding: utf-8 -*-
"""

asyncio liburutegia erabiltzen da pythonez async/await sintaxia darabilen
kode konkurrentea idazteko.

https://docs.python.org/3/library/asyncio.html

"""

import asyncio


async def main():
    print("Hello ...")
    await asyncio.sleep(1)
    print("... world")


asyncio.run(main())
