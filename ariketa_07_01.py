# -*- coding: utf-8 -*-
"""
6. solairuko ordenagailuen informazio bakarra beren IP helbidea da. Gela bakarra dago baina beste informaziorik ez dugu.

Zuzenean Gela eta Ordenagailua motako elementuak sortuko ditugu
"""

from ariketa_01_01 import Gela, Ordenagailua
from utils import inprimatu_gelak


def kargatu_datuak(filename):
    gelak = {}
    with open(filename) as fp:
        datuak = fp.read()
        ip_helbideak = datuak.split(",")
        gela = Gela(601, "601", "601")

        for i, ip_helbidea in enumerate(ip_helbideak):
            ordenagailua = Ordenagailua(
                id=i, kodea=str(i), ip_helbidea=ip_helbidea.strip()
            )
            gela.ordenagailuak.append(ordenagailua)

        gelak[601] = gela

    inprimatu_gelak(gelak)


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_6/ordenagailuak.txt")
