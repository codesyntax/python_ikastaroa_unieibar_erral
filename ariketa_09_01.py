# -*- coding: utf-8 -*-
"""

Orain arte fitxategiak fitxategi sistematik irakurri ditugu.

Orain ordea aldaketa bat egingo dugu. Solairu bakoitzeko arduradunak fitxategiak gure intranetera
kargatzea lortu dugu, beraz orain sarearen bidez deskargatu behar ditugu.

Zerbitzari bat simulatzeko fitxategiak dauzkagun karpetara joan eta zera exekutatuko dugu:

```
$ python3 -m http.server
```

Honek 8000 portuan entzuten duen http zerbitzari sinple bat jarriko du martxan eta karpetaren edukia
http bidez zerbitzatuko du.

Oso tresna erabilgarria da HTMLak nabigatzailean ikusteko (gogoratu JSen murriztapenekin), edo fitxategiak
makina batetik bestera era erraz baten eramateko.

Sarearen bidez fitxategiak irakurtzeko urllib modulua erabiliko dugu

Oinarri bezala 8. ariketako fitxategiak kopiatu eta haren gainean egingo ditugu aldaketak.

"""

from utils import inprimatu_solairua
from ariketa_08_02 import get_solairua as get_solairua_1
from ariketa_08_03 import get_solairua as get_solairua_2
from ariketa_08_04 import get_solairua as get_solairua_3
from ariketa_08_05 import get_solairua as get_solairua_4
from ariketa_08_06 import get_solairua as get_solairua_5
from ariketa_08_07 import get_solairua as get_solairua_6


def inprimatu_solairuak():
    # 1. solairua
    solairua_1 = get_solairua_1()

    # 2. solairua
    solairua_2 = get_solairua_2()

    # 3. solairua
    solairua_3 = get_solairua_3()

    # 4. solairua
    solairua_4 = get_solairua_4()

    # 5. solairua
    solairua_5 = get_solairua_5()

    # 6. solairua
    solairua_6 = get_solairua_6()

    inprimatu_solairua(solairua_1)
    inprimatu_solairua(solairua_2)
    inprimatu_solairua(solairua_3)
    inprimatu_solairua(solairua_4)
    inprimatu_solairua(solairua_5)
    inprimatu_solairua(solairua_6)


def get_solairuak():
    # 1. solairua
    solairua_1 = get_solairua_1()

    # 2. solairua
    solairua_2 = get_solairua_2()

    # 3. solairua
    solairua_3 = get_solairua_3()

    # 4. solairua
    solairua_4 = get_solairua_4()

    # 5. solairua
    solairua_5 = get_solairua_5()

    # 6. solairua
    solairua_6 = get_solairua_6()

    return [solairua_1, solairua_2, solairua_3, solairua_4, solairua_5, solairua_6]


if __name__ == "__main__":
    for solairua in get_solairuak():
        inprimatu_solairua(solairua)
