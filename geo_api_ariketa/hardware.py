"""
Modulo honetan gure logika exekutatuko den hardware-arekin lan egiteko metodoak egongo dira:
    - Pantaila eguneratu (tenperatura erakutsi)
    - Berogailua piztu / itzali
    - Aire girotua piztu / itzali
"""

def set_berogailua(egoera):
    print("Berogailua {} egoeran jarri da.".format(egoera))

def set_aire_girotua(egoera):
    print("Aire girotua {} egoeran jarri da.".format(egoera))

def eguneratu_pantaila(balioa):
    print("Pantaila eguneratuta: {}ºC".format(balioa))