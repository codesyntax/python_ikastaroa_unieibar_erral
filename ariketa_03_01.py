# -*- coding: utf-8 -*-
"""
2. solairura joango gara orain, datuak SQLite datubase baten daude.

Pythonek SQLite datubaseak atzitzeko modulu bat du:

import sqlite3

eta honek DB-API2 izenekoa jarraitzen du, pythonekin datu-baseak atzitzeko modu 'estandarra'.

Beti horrela egiten da:

import datubaseak_atzitzeko_modulua

# Parametroak ezberdinak dira, SQLite, MySQL, PostgreSQL, ... erabiliz gero
con = datubaseak_atzitzeko_modulua.connect("beharrezko parametroak", "hauexek", "dira")
cur = con.cursor()
cur.execute('SQL KONTSULTA')
rows = cur.fetchall()

"""

import sqlite3


def kargatu_datuak(filename):
    con = sqlite3.connect(filename)
    cur = con.cursor()
    cur.execute("SELECT * FROM ORDENAGAILUAK")
    rows = cur.fetchall()
    for row in rows:
        print(row)


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_2/ordenagailuak.db")
