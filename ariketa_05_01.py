# -*- coding: utf-8 -*-
"""

4. solairuko informazioa JSON fitxategi baten dago. Hori pythonen json izeneko moduluarekin kudeatzen
da eta fitxategia irakurrita datu guztiak pythoneko zerrenda eta hiztegien bidez modelatuko dizkigu
automatikoki

"""
import json


def kargatu_datuak(filename):
    datuak = json.load(open(filename))
    print(datuak)


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_4/ordenagailuak.json")
