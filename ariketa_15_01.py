# -*- coding: utf-8 -*-
"""
Orain badugu jada solairu guztien informazioa lortzeko modua, kargatu dezagun guztia
gure MySQL datubasera.

Horretarako records izeneko modulua erabiliko dugu.

Nahiz eta DB-API 2 delakoak datu-basearekiko independentzia eskaintzen zigun, azkenean datu-basearen
konektorearen arabera detaile batzuk desberdinak dira mysql, Oracle edo PostgreSQLn. Hori konpontzeko
records liburutegia erabili dezakegu, honek guztiz abstraituko digu datu-baserako konexioa eta gure kodea
benetan berrerabilgarria izango da, atzeko datu-basea dena-dela

"""

from ariketa_09_01 import get_solairuak

import records

MYSQL_HOST = "127.0.0.1"
MYSQL_USER = "unieibar"
MYSQL_PASS = "unieibar"
MYSQL_DB = "ordenagailuak"


def kargatu_solairuak():
    DB_CONNECTION_STRING = f"mysql://{MYSQL_USER}:{MYSQL_PASS}@{MYSQL_HOST}/{MYSQL_DB}"
    con = records.Database(DB_CONNECTION_STRING)

    for solairua in get_solairuak():
        sql = "INSERT INTO SOLAIRUA (KODEA, IZENA) VALUES (:kodea, :izena)"

        res = con.query(sql, kodea=str(solairua.kodea), izena=str(solairua.izena))

        sql = "SELECT ID from SOLAIRUA WHERE KODEA=:kodea AND IZENA=:izena"

        solairua_rows = con.query(
            sql, kodea=str(solairua.kodea), izena=str(solairua.izena)
        )

        for solairua_row in solairua_rows:
            solairua_id = solairua_row["ID"]
            # solairua_id = solairua_row.ID
            # solairua_id = solairua_row[0]

            for gela in solairua.gelak:
                sql = "INSERT INTO GELA (KODEA, IZENA, SOLAIRUA) VALUES (:kodea, :izena, :solairua)"

                con.query(sql, kodea=gela.kodea, izena=gela.izena, solairua=solairua_id)

                sql = "SELECT ID FROM GELA WHERE KODEA=:kodea and IZENA=:izena and SOLAIRUA=:solairua"

                gela_rows = con.query(
                    sql, kodea=gela.kodea, izena=gela.izena, solairua=solairua_id
                )

                for gela_row in gela_rows:
                    gela_id = gela_row["ID"]
                    # gela_id = gela_row.ID
                    # gela_id = gela_row[0]

                    for ordenagailua in gela.ordenagailuak:
                        sql = "INSERT INTO ORDENAGAILUA(KODEA, IP_HELBIDEA, SISTEMA_ERAGILEA, EROSKETA_EGUNA, AZKEN_ERREBISIOA, OHARRAK, GELA) VALUES (:kodea, :ip_helbidea, :sistema_eragilea, :erosketa_eguna, :azken_errebisioa, :oharrak, :gela)"
                        con.query(
                            sql,
                            kodea=ordenagailua.kodea,
                            ip_helbidea=ordenagailua.ip_helbidea,
                            sistema_eragilea=ordenagailua.sistema_eragilea,
                            erosketa_eguna=ordenagailua.erosketa_eguna,
                            azken_errebisioa=ordenagailua.azken_errebisioa,
                            oharrak=ordenagailua.oharrak,
                            gela=gela_id,
                        )


if __name__ == "__main__":
    kargatu_solairuak()
