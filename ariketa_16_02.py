# -*- coding: utf-8 -*-
"""
Datu-baseko taulak definituta, sortu ditzakun elementuak, zuzenean datu-basean.

Horretarako 9. ariketan prestatutakoa erabiliko dugu adibide gisa

"""

from ariketa_16_01 import Gela
from ariketa_16_01 import Ordenagailua
from ariketa_16_01 import Solairua
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from utils import inprimatu_gela_zerrenda

import codecs
import csv
import urllib.request


BASE_URL = "http://localhost:8000"


def kargatu_datuak(filename):
    ordenagailuak = []
    gelak = {}
    with open(filename) as fp:
        reader = csv.DictReader(fp)
        for i, row in enumerate(reader):
            gela_id = row["Gela"]
            gela = gelak.get(gela_id, None)
            if gela is None:
                gela = Gela(id=gela_id, kodea=str(gela_id), izena=gela_id)
                print(gela)

            ordenagailua = Ordenagailua(
                id=i,
                kodea=row["Ordenagailua"],
                ip_helbidea=row["IP Helbidea"],
                sistema_eragilea=row["Sistema Eragilea"],
                erosketa_eguna=None,
                azken_errebisioa=None,
                oharrak=row["Oharrak"],
            )
            # print(ordenagailua)
            gela.ordenagailuak.append(ordenagailua)
            gelak[gela_id] = gela

    # print(len(gelak.keys()))
    for gela in gelak.values():
        print(
            "{} gelaren ordenagailu kopurua: {}".format(
                gela.id, len(gela.ordenagailuak)
            )
        )


def get_gelak(filename):
    ordenagailuak = []
    gelak = {}
    with open(filename) as fp:
        reader = csv.DictReader(fp)
        for i, row in enumerate(reader):
            gela_id = row["Gela"]
            gela = gelak.get(gela_id, None)
            if gela is None:
                gela = Gela(id=gela_id, kodea=str(gela_id), izena=gela_id)
                print(gela)

            ordenagailua = Ordenagailua(
                id=i,
                kodea=row["Ordenagailua"],
                ip_helbidea=row["IP Helbidea"],
                sistema_eragilea=row["Sistema Eragilea"],
                erosketa_eguna=None,
                azken_errebisioa=None,
                oharrak=row["Oharrak"],
            )
            # print(ordenagailua)
            gela.ordenagailuak.append(ordenagailua)
            gelak[gela_id] = gela

    return gelak.values()


def deskargatu_datuak(helbidea):
    sock = urllib.request.urlopen(self.helbidea)
    reader = csv.DictReader(codecs.iterdecode(sock, "utf-8"))

    gelak = {}
    for i, row in enumerate(reader):
        gela_id = row["Gela"]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=gela_id)

        ordenagailua = Ordenagailua(
            id=i,
            kodea=row["Ordenagailua"],
            ip_helbidea=row["IP Helbidea"],
            sistema_eragilea=row["Sistema Eragilea"],
            erosketa_eguna=None,
            azken_errebisioa=None,
            oharrak=row["Oharrak"],
            gela=gela_id,
        )
        # print(ordenagailua)
        # gela.ordenagailuak.append(ordenagailua)
        gelak[gela_id] = gela

    return gelak.values()


def get_solairua():
    solairua = Solairua(id=1, kodea="1", izena="1. solairua")
    gelak = deskargatu_datuak(f"{BASE_URL}/ordenagailuak.csv")
    for gela in gelak:
        gela.solairua = solairua.id

    return solairua, gelak


class Deskargatzailea(object):
    def __init__(self, session, helbidea):
        self.session = session
        self.helbidea = helbidea
        self.data = self.download_data()

    def download_data(self):
        sock = urllib.request.urlopen(self.helbidea)
        reader = csv.DictReader(codecs.iterdecode(sock, "utf-8"))
        results = []
        for item in reader:
            results.append(item)
        return results

    def get_gelak(self):
        gelak = {}
        for i, row in enumerate(self.data):
            gela_id = row["Gela"]
            gela = gelak.get(gela_id, None)
            if gela is None:
                gela = Gela(id=gela_id, kodea=str(gela_id), izena=gela_id)
                gela.solairua = self.solairua.id
                self.session.add(gela)

                ordenagailua = Ordenagailua(
                    id=i,
                    kodea=row["Ordenagailua"],
                    ip_helbidea=row["IP Helbidea"],
                    sistema_eragilea=row["Sistema Eragilea"],
                    erosketa_eguna=None,
                    azken_errebisioa=None,
                    oharrak=row["Oharrak"],
                    gela=gela_id,
                )
                self.session.add(ordenagailua)

            gelak[gela_id] = gela

        return gelak

    def deskargatu(self):
        self.solairua = Solairua(id=1, kodea="1", izena="1. solairua")
        self.session.add(self.solairua)

        self.get_gelak()

        self.session.commit()


if __name__ == "__main__":
    engine = create_engine("mysql://unieibar:unieibar@127.0.0.1/unieibar2")
    Session = sessionmaker(bind=engine)

    session = Session()
    deskargatzailea = Deskargatzailea(session, f"{BASE_URL}/ordenagailuak.csv")
    deskargatzailea.deskargatu()
