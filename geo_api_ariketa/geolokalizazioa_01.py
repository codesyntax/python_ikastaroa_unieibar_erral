import requests

def lortu_informazioa(herria):
    datuak = None
    api_url = "https://geocoding-api.open-meteo.com/v1/search?name={}".format(herria)
    eskaera = requests.get(api_url)
    if eskaera.status_code == 200:
        datuak = eskaera.json()
    return datuak

def erakutsi_informazioa(datuak):
    hiztegia = {}
    zerrenda = datuak.get('results', [])
    if len(zerrenda) > 0:
        hiztegia = zerrenda[0]
    else:
        print("Ezin izan da informaziorik lortu")
    print("Herria: {}".format(hiztegia.get('name')))
    print("Lat: {} / Lon: {}".format(hiztegia.get('latitude'), hiztegia.get('longitude')))

if __name__ == "__main__":
    herria = input("Idatzi bilatu beharreko herriaren izena: ")
    datuak = lortu_informazioa(herria)
    erakutsi_informazioa(datuak)
