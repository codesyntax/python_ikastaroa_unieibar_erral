# -*- coding: utf-8 -*-
"""
5. solairuko informazioa XML fitxategi baten dator.

Pythonen xml moduluak XML fitxategiak irakurri eta idazteko oinarrizko funtzionalitateak eskaintzen dizkigu

Sortu Gela eta Ordenagailua motako elementuak eta inprimatu

"""

from xml.etree import ElementTree
from ariketa_01_01 import Gela, Ordenagailua, Solairua
from utils import inprimatu_gela_zerrenda, inprimatu_gelak

import urllib.request

BASE_URL = 'http://localhost:8000'

def kargatu_datuak(filename):
    tree = ElementTree.parse(filename)
    root = tree.getroot()
    gelak = {}
    print(root.tag)
    for gela_info in root:
        # kasu honetan XMLaren egitura dela-eta
        # gela barruan dauden ordenagailu guztiak batera lortuko ditugu
        # beraz ez dago Gela motako elementurik hasieran eta zuzenean sortu egingo dugu
        gela = Gela(
            id=gela_info.get("kodea"),
            kodea=str(gela_info.get("kodea")),
            izena=str(gela_info.get("kodea")),
        )

        for ordenagailu_info in gela_info:
            ordenagailua = Ordenagailua(
                id=ordenagailu_info.get("id"),
                kodea=str(ordenagailu_info.get("id")),
                ip_helbidea=ordenagailu_info.find("ip").text,
                sistema_eragilea=ordenagailu_info.find("se").text,
            )
            gela.ordenagailuak.append(ordenagailua)

        gelak[gela_info.get("kodea")] = gela

    inprimatu_gelak(gelak)


def get_gelak(filename):
    tree = ElementTree.parse(filename)
    root = tree.getroot()
    gelak = {}
    print(root.tag)
    for gela_info in root:
        # kasu honetan XMLaren egitura dela-eta
        # gela barruan dauden ordenagailu guztiak batera lortuko ditugu
        # beraz ez dago Gela motako elementurik hasieran eta zuzenean sortu egingo dugu
        gela = Gela(
            id=gela_info.get("kodea"),
            kodea=str(gela_info.get("kodea")),
            izena=str(gela_info.get("kodea")),
        )

        for ordenagailu_info in gela_info:
            ordenagailua = Ordenagailua(
                id=ordenagailu_info.get("id"),
                kodea=str(ordenagailu_info.get("id")),
                ip_helbidea=ordenagailu_info.find("ip").text,
                sistema_eragilea=ordenagailu_info.find("se").text,
            )
            gela.ordenagailuak.append(ordenagailua)

        gelak[gela_info.get("kodea")] = gela

    return gelak.values()


def deskargatu_datuak(helbidea):
    sock = urllib.request.urlopen(helbidea)
    # ElementTreek ere ulertzen du fitxategia zuzenean irakurtzea
    tree = ElementTree.parse(sock)
    root = tree.getroot()
    gelak = {}
    print(root.tag)
    for gela_info in root:
        # kasu honetan XMLaren egitura dela-eta
        # gela barruan dauden ordenagailu guztiak batera lortuko ditugu
        # beraz ez dago Gela motako elementurik hasieran eta zuzenean sortu egingo dugu
        gela = Gela(
            id=gela_info.get("kodea"),
            kodea=str(gela_info.get("kodea")),
            izena=str(gela_info.get("kodea")),
        )

        for ordenagailu_info in gela_info:
            ordenagailua = Ordenagailua(
                id=ordenagailu_info.get("id"),
                kodea=str(ordenagailu_info.get("id")),
                ip_helbidea=ordenagailu_info.find("ip").text,
                sistema_eragilea=ordenagailu_info.find("se").text,
            )
            gela.ordenagailuak.append(ordenagailua)

        gelak[gela_info.get("kodea")] = gela

    return gelak.values()


def get_solairua():
    solairua = Solairua(id=5, kodea="5", izena="5. solairua")
    solairua.gelak = deskargatu_datuak(f"{BASE_URL}/ordenagailuak.xml")
    return solairua


if __name__ == "__main__":
    solairua = get_solairua()
    inprimatu_gela_zerrenda(solairua.gelak)
