# -*- coding: utf-8 -*-
"""
Aurreko pausoko fitxategia emailez bidali


"""
from email.message import EmailMessage
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from io import BytesIO
from os.path import basename

import datetime
import hashlib
import random
import smtplib
import string
import subprocess
import tempfile
import urllib.request
import zipfile


BASE_URL = 'http://localhost:8000'

solairua_1_file = 'ordenagailuak.csv'
solairua_2_file = 'ordenagailuak.db'
solairua_3_file = 'ordenagailuak.ini'
solairua_4_file = 'ordenagailuak.json'
solairua_5_file = 'ordenagailuak.xml'
solairua_6_file = 'ordenagailuak.txt'


def deskargatu_fitxategiak(fitxategiak):
    # lehenengo karpeta tenporal bat sortuko dugu eta hara deskargatu
    # Kontuz ze erabiltzeari uzten diogunean direktorio tenporala ezabatu egingo da.
    directory = tempfile.TemporaryDirectory()

    for fitxategia in fitxategiak:
        sock = urllib.request.urlopen(f'{BASE_URL}/{fitxategia}')
        with open(directory.name + '/' + fitxategia, 'wb') as fp:
            fp.write(sock.read())

    # s = BytesIO()

    #with zopefile.ZipFile(s, 'w') as zf:

    data= datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    ausazko_karaktereak = random.sample(string.ascii_letters, 10)
    ausazko_katea = ''.join(ausazko_karaktereak)

    hasha = hashlib.sha256(ausazko_katea.encode('utf-8')).hexdigest()
    izena = '{}-{}'.format(data, hasha)

    konprimatutako_fitxategi_izena = f"/tmp/{izena}.zip"

    konprimatutako_fitxategia = zipfile.ZipFile(konprimatutako_fitxategi_izena, 'w')
    for filename in fitxategiak:
        konprimatutako_fitxategia.write(directory.name + '/' + filename)


    msg = MIMEMultipart()
    msg['From'] = 'zerbitzaria@uni.eus'
    msg['To'] = 'informatika@uni.eus'
    msg['Subject'] = 'Gaurko ordenagailuen fitxategiak - {}'.format(data)

    text = 'Hona hemen gaurko fitxategiak'

    msg.attach(MIMEText(text))


    with open(konprimatutako_fitxategi_izena, "rb") as fil:
        part = MIMEApplication(
            fil.read(),
            Name=basename(konprimatutako_fitxategi_izena)
        )
    # After the file is closed
    part['Content-Disposition'] = 'attachment; filename="%s"' % basename(konprimatutako_fitxategi_izena)
    msg.attach(part)


    smtp = smtplib.SMTP('localhost')
    smtp.sendmail('zerbitzaria@uni.eus', "informatika@uni.eus", msg.as_string())
    smtp.close()

if __name__ == '__main__':
    fitxategiak = [solairua_1_file, solairua_2_file, solairua_3_file, solairua_4_file, solairua_5_file, solairua_6_file]
    deskargatu_fitxategiak(fitxategiak)
