# -*- coding: utf-8 -*-
"""

Dekoradoreak (decorators), funtzio eta klaseen funtzionamendua guztiz aldatu gabe funtzionalitate gehigarriak eskaintzen
dituzten tresnak dira.

Ulertzeko konplexuak izan daitezke, parametro bezala funtzio bat hartzen duten funtzioak (!!!) direlako.

Baina hori ez da arazoa guretzat, python-en guztia objektuak dira-eta.

"""

"""
Adibidea: funtzio baten exekuzioak zenbat denbora eramaten duen jakin nahi dugu. Funtzioa hauxe da:
"""


def funtzio_luze_luzea():
    import random
    import time

    segundoak = random.randint(0, 6)
    time.sleep(segundoak)


if __name__ == "__main__":
    funtzio_luze_luzea()
