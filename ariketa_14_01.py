# -*- coding: utf-8 -*-
"""
Jada badugu solairu guztien informazioa lortzeko modua.

Orokortu dezagun, beraz, ariketa_08_01 fitxategia solairu guztien informazioa get_solairuak izeneko funtzio batekin lortzeko


"""

from utils import inprimatu_solairua


def inprimatu_solairuak():
    # 1. solairua
    from ariketa_08_02 import get_solairua

    solairua_1 = get_solairua()

    # 2. solairua
    from ariketa_08_03 import get_solairua

    solairua_2 = get_solairua()

    # 3. solairua
    from ariketa_08_04 import get_solairua

    solairua_3 = get_solairua()

    # 4. solairua
    from ariketa_08_05 import get_solairua

    solairua_4 = get_solairua()

    # 5. solairua
    from ariketa_08_06 import get_solairua

    solairua_5 = get_solairua()

    # 6. solairua
    from ariketa_08_07 import get_solairua

    solairua_6 = get_solairua()

    inprimatu_solairua(solairua_1)
    inprimatu_solairua(solairua_2)
    inprimatu_solairua(solairua_3)
    inprimatu_solairua(solairua_4)
    inprimatu_solairua(solairua_5)
    inprimatu_solairua(solairua_6)


def get_solairuak():
    # 1. solairua
    from ariketa_08_02 import get_solairua

    solairua_1 = get_solairua()

    # 2. solairua
    from ariketa_08_03 import get_solairua

    solairua_2 = get_solairua()

    # 3. solairua
    from ariketa_08_04 import get_solairua

    solairua_3 = get_solairua()

    # 4. solairua
    from ariketa_08_05 import get_solairua

    solairua_4 = get_solairua()

    # 5. solairua
    from ariketa_08_06 import get_solairua

    solairua_5 = get_solairua()

    # 6. solairua
    from ariketa_08_07 import get_solairua

    solairua_6 = get_solairua()

    return [solairua_1, solairua_2, solairua_3, solairua_4, solairua_5, solairua_6]


if __name__ == "__main__":
    for solairua in get_solairuak():
        inprimatu_solairua(solairua)
