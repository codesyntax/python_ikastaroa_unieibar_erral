from geolokalizazioa_02 import (
    lortu_geo_informazioa,
    lortu_eguraldi_informazioa,
    erakutsi_geo_informazioa,
)
from hardware import (
    eguneratu_pantaila,
    set_berogailua,
    set_aire_girotua,
)

if __name__ == "__main__":
    herria = input("Idatzi bilatu beharreko herriaren izena: ")
    tenp_min = input("Idatzi berogailua pizteko gutxieneko tenperatura: ")
    tenp_max = input("Idatzi aire girotua pizteko gehieneko tenperatura: ")
    datuak = lortu_geo_informazioa(herria)
    erakutsi_geo_informazioa(datuak)
    eguraldi_datuak = lortu_eguraldi_informazioa(
        datuak.get('latitude'), datuak.get('longitude')
    )
    # akzioak abiarazi: kasu honetan, soilik pantaila eguneratu
    graduak = eguraldi_datuak.get('current_weather', {}).get('temperature')
    eguneratu_pantaila(graduak)
    piztu_berogailua = False
    piztu_aire_girotua = False
    if float(graduak) < float(tenp_min):
        piztu_berogailua = True
    if float(graduak) > float(tenp_max):
        piztu_aire_girotua = True
    
    set_berogailua(piztu_berogailua)
    set_aire_girotua(piztu_aire_girotua)