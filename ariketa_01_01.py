# -*- coding: utf-8 -*-
"""
Definitu ditzagun gure datu-baseko elementuak errepresentatzeko datu-motak
"""

from dataclasses import dataclass
from dataclasses import field
from datetime import date


@dataclass
class Departamentua:
    id: int
    kodea: str
    izena: str


@dataclass
class Gela:
    id: int
    kodea: str
    izena: str
    ordenagailuak: list = field(default_factory=list)


@dataclass
class Solairua:
    id: int
    kodea: str
    izena: str


@dataclass
class Ordenagailua:
    id: int
    kodea: str
    ip_helbidea: str
    sistema_eragilea: str = None
    erosketa_eguna: date = None
    azken_errebisioa: date = None
    oharrak: str = None


if __name__ == "__main__":
    s1 = Solairua(id=1, kodea="1", izena="1. solairua")
    s2 = Solairua(id=2, kodea="2", izena="2. solairua")
    s3 = Solairua(id=3, kodea="3", izena="3. solairua")

    print(s1)
