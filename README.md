# Enuntziatua

Gure ikastetxeko ordenagailu guztien informazioa jasotzen duen datu-base bat osatzea erabaki dugu
ordenagailu guzti horien informazioa gorde eta era errazean kudeatzeko.

Datu-base hori gaur egun ez da existitzen baina gure ikastetxeko departamentu bakoitzak badu bakoitzari esleituta
dauden informatika geletako ordenagailuen informazioa. Zoritxarrez informazio hori formatu ezberdinetan gorde izan
dute departamentuetako IKT arduradunek eta guri dagokigu informazio hori bateratzea.

Gure lana, ordea, ez da datu-guztien informazioa batuko duen datu-basea diseinatzea izango, datu-base ezberdin guzti
horietan gaur egun dagoen informazioa irakurri, prozesatu, eraldatu eta datu-base berrian txertatzea baizik.

Datu-base berriaren diseinua osatuta dugu eta hauxe izango da:

- MySQL datu-base bat izango da
- Taula hauexek izango ditu, bakoitza bertan adierazten den zutabeekin:

  Solairua

  - Kodea: VARCHAR
  - Izena: VARCHAR

  Gela

  - Solairua: Foreign Key (Solairua)
  - Departamentua: Foreign Key (Departamentua)
  - Kodea: VARCHAR
  - Izena: VARCHAR

  Ordenagailua:

  - Gela: Foreign Key (Gela)
  - Kodea: VARCHAR
  - IP Helbidea: VARCHAR
  - Sistema Eragilea: VARCHAR
  - Erosketa eguna: DATETIME
  - Azken errebisioa: DATETIME
  - Oharrak: VARCHAR

Solairu bakoitzeko ordenagailuen datuak formatu ezberdinetan daude, hona hemen horien azalpena:

1. Solairua: CSV fitxategi baten
2. Solairua: SQLite datu-base baten
3. Solairua: INI fitxategi baten
4. Solairua: JSON fitxtegi baten
5. Solairua: XML fitxategi baten
6. Solairua: ez dago informaziorik, IP zerrenda bakarrik daukagu TXT fitxategi baten, baina guztiak Linux dira eta zorionez ikastetxeko linux guztietara konektatzeko root erabiltzailearen pasahitz berbera erabiltzen dugu.

Fitxategi guztien ereduak ditugu eskuragai.

# Liburutegi estandarra erabiliz egiteko ariketak

## Ariketa 01

dataclasses modulua erabiliz datu-motak definitu


## Ariketa 02

csv modulua erabiliz fitxategi baten edukiak irakurri eta aurrekoan sortutako datu-motak sortu


## Ariketa 03

sqlite3 modulua erabiliz SQLite datu-base baten edukiak irakurri eta datu-motak sortu. DB-API


## Ariketa 04

configparser modulua erabiliz ini fitxategia irakurri, datu-motak sortu eta inprimatzeko funtzio orokor bat egin


## Ariketa 05
json modulua erabiliz json fitxategia irakurri, datu-motak sortu, funtzio orokorra erabili eta datak prozesatu.


## Ariketa 06
xml modulua erabiliz XML fitategi bat parseatu

## Ariketa 07
txt fitxategi sinple bat irakurri

## Ariketa 08

Aurreko ariketak orokortu bakoitzak berari dagokion Solairua motako elementu bat itzultzeko


## Ariketa 09

Aurreko ariketetako fitxategiak fitxategi sistematik irakurri beharren http eskaera bat eginez deskargatu (`python -m http.server` eta `urllib`)


## Ariketa 10

Fitxtegiak lokalean karpeta baten gorde eta sistema tar komandoa erabiliz konprimatu (subprocess, os eta sys moduluak).

## Ariketa 11

Fitxategiak http eskaera bat egitean lokalean karpeta baten gorde. Fitxategi guzti horiek konprimatutako fitxategi baten gorde behar dira python bakarrik erabiliz (gzip edo zipfile modulua) 


## Ariketa 12

Aurreko pausoan sortu dugun fitxategiaren izena, deskarga bakoitzeko fitxategiak bereizteko, momentuko data eta orduarekin eta fitxategiaren beraren hash bat erabiliz sortu beharko da (hashlib modulua)

## Ariketa 13

Aurreko pausoan sortu dugun fitxategia, fitxategi sisteman gordetzeaz gain epostaz bidaliko da helbide jakin batera. Erabili [MailHog](https://github.com/mailhog/MailHog) probatarako SMTP zerbitzari bezala


# Pakete gehigarriak instalatuz egiteko ariketak

## Ariketa 14

8. ariketak itzulitako solairuak MySQL datu-base baten idatzi. mysqlclient liburutegi gehigarria instalatu (pip install)


## Ariketa 15

8. ariketak itzulitako solairuak MySQL datu-base baten idatzi records liburutegi gehigarria instalatuz


## Ariketa 16

SQLAlchemy modulua erabiliz ORM bat erabiltzen ikasiko dugu, horrela datubaseari ez dizkiogu eskaerak zuzenean egin behar, python objektuekin arituko gara eta SQLAlchemy arduratuko da datu-basean idazteaz.

Horretarako 1. ariketan definitu dugun klasea berrerabiliko dugu


## Ariketa 17

Internetetik datuak deskargatzeko urllib erabili beharrean requests izeneko liburutegia erabiliko dugu, askoz erosoagoa delako.


## Ariketa 18

CSV fitxategia Excel fitxategi batekin ordezkatu dute.


# Pythonen ezaugarri aurreratuak erabili

## Ariketa 19

`typehint` edo aldagai eta funtzioen datu-motak


## Ariketa 20

Dekoradoreak


## Ariketa 21

Programazio asinkronoa/konkurrentea

## Geo-API ariketa
Bi API publikotatik herri baten latitude eta longitudea eta uneko tenperatura atera, horren arabera hardware (Raspberry) batean akzioak abiarazteko (berogailua piztu/itzali, aire girotua piztu/itzali)