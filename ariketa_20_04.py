# -*- coding: utf-8 -*-
"""

Baina hau ez da txukuna, funtzioa kakaztu egin dugu denbora kalkulu horiek sartuz.

Orduan dekoradore bat prestatu dezakegu eta funtzioari asignatu:

"""

import time


def timeit(funtzioa):
    """funtzio bat parametro bezala hartu eta funtzio bat itzultzen duen funtzioa da dekoradorea (!!)
    hau da, funtzio berri bat sortuko dugu hasiera eta bukaeraren kalkulua egin eta jatorrizko funtzioari
    deitzen diona (dagozkion parametro guztiak pasatuz, ikusi *args eta **kw),
    eta funtzio berri hori itzuliko dugu
    """

    def timed(*args, **kw):
        hasiera = time.time()
        emaitza = funtzioa(*args, **kw)
        bukaera = time.time()

        print(
            "%r (%r, %r) %2.2f sec" % (funtzioa.__name__, args, kw, bukaera - hasiera)
        )
        return emaitza

    return timed


@timeit
def funtzio_luze_luzea():
    import random
    import time

    segundoak = random.randint(0, 6)
    time.sleep(segundoak)


if __name__ == "__main__":
    funtzio_luze_luzea()
