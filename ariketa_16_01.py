# -*- coding: utf-8 -*-
"""
Definitu ditzagun gure datu-baseko elementuak errepresentatzeko datu-motak

"""

from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import relationship


# declarative base class
Base = declarative_base()


class Gela(Base):
    __tablename__ = "gela"

    id = Column(Integer, primary_key=True)
    kodea = Column(String(255))
    izena = Column(String(255))
    ordenagailuak = relationship("Ordenagailua")
    solairua = Column(Integer, ForeignKey("solairua.id"))


class Solairua(Base):
    __tablename__ = "solairua"

    id = Column(Integer, primary_key=True)
    kodea = Column(String(255))
    izena = Column(String(255))
    gelak = relationship("Gela")


class Ordenagailua(Base):
    __tablename__ = "ordenagailua"

    id = Column(Integer, primary_key=True)
    kodea = Column(String(255))
    ip_helbidea = Column(String(255))
    sistema_eragilea = Column(String(255))
    erosketa_eguna = Column(String(255))
    azken_errebisioa = Column(String(255))
    oharrak = Column(String(255))
    gela = Column(Integer, ForeignKey("gela.id"))


if __name__ == "__main__":
    # datu-basera konektatu
    engine = create_engine("mysql://unieibar:unieibar@127.0.0.1/unieibar2")
    # datu-baseko taulak sortu
    Base.metadata.create_all(engine)
