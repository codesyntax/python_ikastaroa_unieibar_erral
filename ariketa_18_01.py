# -*- coding: utf-8 -*-
"""
1. solairuko irakaslea bajan egon den bitartean haren ordezkoak CSV fitxategia Excel fitxategi batekin ordezkatu du eta gure tresnan sartzeko eskaera egin du

2. Horretarako openpyxl modulua erabiliko dugu

"""


from ariketa_01_01 import Gela
from ariketa_01_01 import Ordenagailua
from ariketa_01_01 import Solairua

from openpyxl import load_workbook


def kargatu_datuak(filename):
    wb = load_workbook(filename)
    ws = wb.active
    ordenagailuak = []
    gelak = {}

    for i, row in enumerate(ws):
        gela_id = row[0].value
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=gela_id)
            print(gela)

        ordenagailua = Ordenagailua(
            id=i,
            kodea=row[1].value,
            ip_helbidea=row[2].value,
            sistema_eragilea=row[3].value,
            erosketa_eguna=None,
            azken_errebisioa=None,
            oharrak=row[4].value,
        )
        # print(ordenagailua)
        gela.ordenagailuak.append(ordenagailua)
        gelak[gela_id] = gela

    # print(len(gelak.keys()))
    for gela in gelak.values():
        print(
            "{} gelaren ordenagailu kopurua: {}".format(
                gela.id, len(gela.ordenagailuak)
            )
        )


if __name__ == "__main__":
    kargatu_datuak("./datuak/solairua_1/ordenagailuak.xlsx")
