# -*- coding: utf-8 -*-
"""
SQLite datubasetik sortu orain Gela eta Ordenagailua objektuak

erabili 2. ariketako 3. fitxategiak erabili dugun hiztegia elementuak gordetzeko

"""


from ariketa_01_01 import Gela
from ariketa_01_01 import Ordenagailua
from ariketa_01_01 import Solairua
from utils import inprimatu_gela_zerrenda

import sqlite3
import urllib.request
import tempfile

BASE_URL = 'http://localhost:8000'

def kargatu_datuak(filename):
    con = sqlite3.connect(filename)
    cur = con.cursor()
    cur.execute("SELECT * FROM ORDENAGAILUAK")
    rows = cur.fetchall()

    gelak = {}

    for row in rows:
        gela_id = row[1]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=str(gela_id))
            print(gela)

        ordenagailua = Ordenagailua(
            id=row[0],
            kodea=str(row[0]),
            ip_helbidea=row[2],
            sistema_eragilea=row[2],
            oharrak=row[4],
        )

        gela.ordenagailuak.append(ordenagailua)
        gelak[gela_id] = gela

    for gela in gelak.values():
        print(
            "{} gelaren ordenagailu kopurua: {}".format(
                gela.id, len(gela.ordenagailuak)
            )
        )


def get_gelak(filename):
    con = sqlite3.connect(filename)
    cur = con.cursor()
    cur.execute("SELECT * FROM ORDENAGAILUAK")
    rows = cur.fetchall()

    gelak = {}

    for row in rows:
        gela_id = row[1]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=str(gela_id))
            print(gela)

        ordenagailua = Ordenagailua(
            id=row[0],
            kodea=str(row[0]),
            ip_helbidea=row[2],
            sistema_eragilea=row[2],
            oharrak=row[4],
        )

        gela.ordenagailuak.append(ordenagailua)
        gelak[gela_id] = gela

    return gelak.values()


def deskargatu_datuak(helbidea):
    sock = urllib.request.urlopen(helbidea)

    # SQLITE moduluak fitxategi batetik egin behar du irakurketa
    # beraz fitxategi tenporal baten gorde dezakegu edukia eta gero
    # huraxe erabili
    tmp = tempfile.NamedTemporaryFile()
    tmp.file.write(sock.read())

    con = sqlite3.connect(tmp.name)
    cur = con.cursor()
    cur.execute("SELECT * FROM ORDENAGAILUAK")
    rows = cur.fetchall()

    gelak = {}

    for row in rows:
        gela_id = row[1]
        gela = gelak.get(gela_id, None)
        if gela is None:
            gela = Gela(id=gela_id, kodea=str(gela_id), izena=str(gela_id))
            print(gela)

        ordenagailua = Ordenagailua(
            id=row[0],
            kodea=str(row[0]),
            ip_helbidea=row[2],
            sistema_eragilea=row[2],
            oharrak=row[4],
        )

        gela.ordenagailuak.append(ordenagailua)
        gelak[gela_id] = gela

    # irekitakoa itxi
    tmp.close()
    return gelak.values()


def get_solairua():
    solairua = Solairua(id=2, kodea="2", izena="2. solairua")
    solairua.gelak = deskargatu_datuak(f"{BASE_URL}/ordenagailuak.db")
    return solairua


if __name__ == "__main__":
    solairua = get_solairua()
    inprimatu_gela_zerrenda(solairua.gelak)
