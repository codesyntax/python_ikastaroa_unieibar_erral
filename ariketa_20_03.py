# -*- coding: utf-8 -*-
"""
Baina funtzio horri leku askotan deitzen badiogu, oso astuna izan daiteke
dei guztien aurretik eta ondoren denboraren kalkulua gehitzea.

Beraz, kalkulua funtzioaren barrura eramatea pentsatu dezakegu, horrela:
s
"""


def funtzio_luze_luzea():
    import time

    hasiera = time.time()
    import random

    segundoak = random.randint(0, 6)
    time.sleep(segundoak)
    bukaera = time.time()
    print("funtzioaren iraupena: {} segundo".format(bukaera - hasiera))


if __name__ == "__main__":
    funtzio_luze_luzea()
